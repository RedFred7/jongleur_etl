require 'jongleur'
require_relative './jongleur_tasks'
include Jongleur

test_graph = {
  Extract1: [:Transform1],
  Transform1: [:Load1],
  Extract2: [:Transform2],
  Transform2: [:Load2],
  Load1: [:Cleanup],
  Load2: [:Cleanup]
}

API.add_task_graph test_graph
# graph = API.print_graph('/tmp')
# system "open #{graph}"


API.run do |on|
  on.completed do |task_matrix|
    puts "Jongleur run is complete \n"
    puts task_matrix
    puts "oh-oh" if API.failed_tasks(task_matrix).length > 0
  end
end
