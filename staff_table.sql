CREATE TABLE public.staff
(
    id integer NOT NULL DEFAULT nextval('staff_id_seq'::regclass),
    first_name text COLLATE pg_catalog."default" NOT NULL,
    last_name text COLLATE pg_catalog."default" NOT NULL,
    email character varying COLLATE pg_catalog."default",
    staff_no uuid NOT NULL,
    CONSTRAINT staff_pkey PRIMARY KEY (id)
);